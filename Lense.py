# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
fig, ax = plt.subplots()
class Lense:
    def __init__(self, R1, R2, d, D, n, sc):
        self.R1 = R1
        self.R2 = R2
        self.d = d
        self.D = D
        self.n = n
        self.sc = sc
    

    def Lense (self):
        if self.R1 > 0 and self.R2 < 0:
            teta1 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R1)))
            teta2 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R2)))
            arc1 = plt.matplotlib.patches.Arc([self.sc[0]+self.R1, 0], 2*abs(self.R1), 2*abs(self.R1), 0.0, 180-teta1, 180+teta1)
            arc2 = plt.matplotlib.patches.Arc([self.sc[0]+self.R2+self.d, 0], 2*abs(self.R2), 2*abs(self.R2), 0.0, -teta2, teta2)
            ax.add_artist(arc1)
            ax.add_artist(arc2)
        if self.R1 < 0 and self.R2 > 0:
            teta1 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R1)))
            teta2 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R2)))
            arc1 = plt.matplotlib.patches.Arc([self.sc[0]+self.R1, 0], 2*abs(self.R1), 2*abs(self.R1), 0.0, -teta1, teta1)
            arc2 = plt.matplotlib.patches.Arc([self.sc[0]+self.R2+self.d, 0], 2*abs(self.R2), 2*abs(self.R2), 0.0, 180-teta2, 180+teta2)
            ax.add_artist(arc1)
            ax.add_artist(arc2)
        if self.R1 < 0 and self.R2 < 0:
            teta1 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R1)))
            teta2 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R2)))
            arc1 = plt.matplotlib.patches.Arc([self.sc[0]+self.R1, 0], 2*abs(self.R1), 2*abs(self.R1), 0.0, -teta1, teta1)
            arc2 = plt.matplotlib.patches.Arc([self.sc[0]+self.R2+self.d, 0], 2*abs(self.R2), 2*abs(self.R2), 0.0, -teta2, teta2)
            ax.add_artist(arc1)
            ax.add_artist(arc2)
        if self.R1 > 0 and self.R2 > 0:
            teta1 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R1)))
            teta2 = np.rad2deg(np.arcsin((self.D/2)/abs(self.R2)))
            arc1 = plt.matplotlib.patches.Arc([self.sc[0]+self.R1, 0], 2*abs(self.R1), 2*abs(self.R1), 0.0, 180-teta1, 180+teta1)
            arc2 = plt.matplotlib.patches.Arc([self.sc[0]+self.R2+self.d, 0], 2*abs(self.R2), 2*abs(self.R2), 0.0, 180-teta2, 180+teta2)
            ax.add_artist(arc1)
            ax.add_artist(arc2)
        #print [self.sc[0]+self.R1, 0], 2*abs(self.R1), 2*abs(self.R1), 0.0, 180-teta1, 180+teta1
        #print[self.sc[0]+self.R2+self.d, 0], 2*abs(self.R2), 2*abs(self.R2), 0.0, -teta2, teta2
        #if material == 'L':
           # x1 = [-np.sqrt(self.R1**2 - (self.D/2)**2)+(self.sc[0]+self.R1), np.sqrt(self.R2**2 - (self.D/2)**2)+(self.sc[0]+self.R2+self.d)]
        #    y1 = [self.D/2, self.D/2]    
        #    x2 = [-np.sqrt(self.R1**2 - (self.D/2)**2)+(self.sc[0]+self.R1), np.sqrt(self.R2**2 - (self.D/2)**2)+(self.sc[0]+self.R2+self.d)]
        #    y2 = [-self.D/2, -self.D/2]
       #     plt.plot(x1, y1)
        #    plt.plot(x2, y2)
            #plt.show()
       #     print -np.sqrt(self.R1**2 - (self.D/2)**2)+(self.sc[0]+self.R1), np.sqrt(self.R2**2 - (self.D/2)**2)+(self.sc[0]+self.R2+self.d)
