import matplotlib.pyplot as plt
import random as rd
print ('Enter minimum: ')
mn=input()
print ('Enter maximum')
mx=input()
D=[mn,mx]
print('Enter the number of intervals: ')
N=input()
print ('Enter the number of implementations: ')
m=input()
y=[]
k=[]
for i in range(N):
    k.append(0) 
for i in range(m):
    y.append(rd.randint(mn, mx))
def isInBin(v,j,N,D):
    delta=(D[1]-D[0])/N
    low=D[0]+delta*j
    high=D[0]+delta*(j+1)
    ret=low<v<=high or (v==D[0] and j==0)
    return ret
for v in y:
    for j in range(N):
         if isInBin(v,j,N,D):
             k[j]+=1
b=0
for i in k:
   b=b+i
print 'check: ', b
hist=[]
for i in range(N):
    k[i]=float(k[i])
    hist.append(k[i]/m)
    hist[i]=round (hist[i],3)
#print k
#print hist
plt.plot(range(0, m, (m/N)),hist)
plt.show()