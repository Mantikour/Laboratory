# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
from Surface import *

#ax.axis([-10,10, -50,50])
ax.grid(True, which='both')
n = [1.0, 2.0, 1.0, 2.0, 1.0, 1.33, 2.0, 1.0]
R = [7.0, -7.0, 7.0, -10.0, 7.0]
cx = [0.0, 15, 30.0, 50.0, 70.]
cy = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
vec = [-4.0, 0.0]
start = [-20.0, 3.0]
s = []
x = [start[0]]
y = [start[1]]
#x_refl = [start[0]]
#y_refl = [start[1]]
#s1 = Surface(7, [1, 0], 1, 2)
for i in range(np.size(R)):
    s.append(Surface(R[i], [cx[i], cy[i]], n[i], n[i+1]))
    s[i].Circle()
    
for i in range(np.size(R)):
    k = s[i].Line(vec, start)[0]
    b = s[i].Line(vec, start)[1]
    point = s[i].intersection(vec, start)
    if point == 'don`t cross':
        #x.append(50)
        #y.append(k*x[np.size(x)-1]+b)
        #ax.plot(x, y)
        print 'don`t cross'
        break
    x.append(point[0])
    y.append(point[1])
    kn = s[i].getN(point)[0]
    bn = s[i].getN(point)[1]
    alpha = s[i].phi(k, kn)
    k_refl = s[i].Reflection(kn, point, alpha)[0]
    b_refl = s[i].Reflection(kn, point, alpha)[1]
    if s[i].PVO(alpha) == 1:
        #k_refl = s[i].Reflection(kn, point, alpha)[0]
        #b_refl = s[i].Reflection(kn, point, alpha)[1]
        s[i].plotRefl(k_refl, b_refl, point)
        print 'Total internal reflection on the', i+1, 'surface'
        break
        
    beta = s[i].beta(alpha)
    k_refr = s[i].Refraction(kn, point, beta)[0]
    b_refr = s[i].Refraction(kn, point, beta)[1]
    k = k_refr
    b = b_refr
    vec = s[i].create_vec(k_refr, b_refr, point)
    start = point
    #s[i].plotN(kn, bn, point)
    s[i].plotRefl(k_refl, b_refl, point)

if (i == np.size(R)-1) or i == 0:
    x.append(100)
    y.append(k*x[np.size(x)-1]+b)
    plt.plot(x, y)
else:
    plt.plot(x, y)
#ax.plot(x_refl, y_refl)
plt.show()
#print s[0].check()