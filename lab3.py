import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin

n1 = 1.0
n2 = 2.33
K1 = -n1/n2
gr = 60                       #угол падения
angle = float((gr) * np.pi/180)
beta = asin(sin(angle) * n1/n2)
plt.axis('equal')
plt.grid(True, which='both')
plt.axvline(0, c="none")
plt.axhline(0, c="black")
plt.axhline(l, c="black")
def plotRay(vec, pt=[0.0, 0.0], show=True):
    plt.plot([pt[0], vec[0]], [pt[0], vec[1]])
    if show:
        plt.show()
        
#def plotRayPP(vec, pt=[np.tan(beta), l], show=True):
    plt.plot([pt[0], vec[0]], [pt[0], vec[1]])
    if show:
        plt.show()
        
def PP(angle):
    n = np.array([l*np.tan(beta),l])
    plotRay(n, pt=[l*np.tan(beta), l], show=False)
    
def Rays(angle): 
    normal = np.array ([0.0,1.0])
    d = np.array([np.sin(angle), np.cos(angle)])
    r = (-d - 2*(np.dot(-d, normal)*normal)) 
    C = -np.dot(normal, d)
    q1 = (-l/np.cos(beta))*(K1*d + (K1*C - np.sqrt(1-pow(K1, 2)*(1-pow(C, 2))))*normal)
    plotRay(normal, show=False)
    plotRay(d, show=False)
    plotRay(r, show=False)
    plotRay(q1, show=False)

def PVO(angle):
    normal = np.array ([0.0, 1.0])
    d = np.array([np.sin(angle), np.cos(angle)])
    r = -d - 2*(np.dot(-d, normal))*normal 
    plotRay(normal, show=False)
    plotRay(d, show=False)
    plotRay(r)  
if n1>n2 and np.sin(angle)>=n2/n1:
    PVO(angle)
else:
    Rays(angle)
#y=kx+b
#y=l