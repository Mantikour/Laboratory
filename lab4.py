import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
n1 = 1.33
n2 = 2.64
n3 = 1.0
K1 = -n1/n2
K2 = -n2/n3
gr =30                    #угол падения
l=-1
y = float(l)
angle = float((gr) * np.pi/180)
beta = asin(sin(angle) * n1/n2)
plt.axis('equal')
plt.grid(True, which='both')
plt.axvline(0, c="none")
plt.axhline(0, c="black")
plt.axhline(l, c="black")
def plotRay(vec, pt=[0.0, 0.0], show=True):
    plt.plot([pt[0], vec[0]], [pt[1], vec[1]])
    if show:
        plt.show()
    
def Rays(angle, K): 
    global normal, r, q, d
    normal = np.array ([0.0,1.0])
    d = np.array([np.sin(angle), np.cos(angle)])
    r = (-d - 2*(np.dot(-d, normal)*normal)) 
    C = -np.dot(normal, d)
    q = (K*d + (K*C - np.sqrt(1-pow(K, 2)*(1-pow(C, 2))))*normal)
    
def PVO(angle):
    global normal, r, q, d
    normal = np.array ([0.0, 1.0])
    d = np.array([np.sin(angle), np.cos(angle)])
    r = -d - 2*(np.dot(-d, normal))*normal 
  
if n1>n2 and np.sin(angle)>=n2/n1:
    PVO(angle)
    #plotRay(normal, show=False)
    plotRay(d, show=False)
    plotRay(r, show=False)
else:
    Rays(angle, K1)
    #plotRay(normal, show=False)
    plotRay(d, show=False)
    plotRay(r, show=False)
    plotRay((-l/np.cos(beta))*q, show=False)
if n2>n3 and np.sin(beta)>=n3/n2:
    PVO(beta)
    #plotRay(normal+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
    plotRay(r+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
else:
    Rays(beta, K2)
    #plotRay((-l/np.cos(beta))*normal+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
    plotRay((-l/np.cos(beta))*r+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
    plotRay(q+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
