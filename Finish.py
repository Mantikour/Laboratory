# -*- coding: utf-8 -*-# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
from Lense import *
from Surface import *
M = 200
sc = [0.0, 0.0]
n = [1.0, 1.5, 1.0, 1.33, 1.0]#, 1.33, 1.0, 1.33, 1.0]
R = [50.0, -65.0, 50.0, -65.0]#, -90.0, 40.0, 50.0, 40.0]
d = [10.0, 30.0, 10.0, 5.0]#, 10.0, 5.0, 5.0, 5.0]
D = [30.0, 30.0, 30.0, 30.0]#, 30.0, 30.0, 30.0, 30.0]
vec0 = [3.0, 2.0]
zero = [-60.0, 0.0]
#vec0 = [3.0, 0.0]
#zero = [-10., 14.0]
start = zero
POI = 150.0
step = 150.0
cx =[]
cy = []
L = []
s = []
k_check = []
beta_check = []
alpha_check = []
for i in range(np.size(R)-1):                                       
    L.append(Lense(R[i], R[i+1], d[i], D[i], n[i+1], sc))
    L[i].Lense()
    sc[0] = sc[0]+d[i]
cc = 0
dd = 0

for i in range (np.size(R)):
    cc = dd + R[i]
    cx.append(cc)
    cy.append(0.0) 
    dd = dd + d[i]
    
for i in range(np.size(R)):
    s.append(Surface(R[i], [cx[i], cy[i]], n[i], n[i+1], D[i]/2))

def SpotD(R, surface, vec0, zero, POI, show=False, N=100):
    vec = vec0
    star = zero
    picture = []
    for j in range(N):
        star = zero
        x = [star[0]]
        y = [star[1]]
        for i in range(np.size(R)):
            [k, b] = surface[i].Line(vec, star)
            point = surface[i].intersection(vec, star)
            if point == 'don`t cross':
                #print 'don`t cross'
                break
            x.append(point[0])
            y.append(point[1])
            [kn, bn] = surface[i].getN(point)
            alpha = surface[i].phi(k, kn)
            [k_refl, b_refl] = surface[i].Reflection(kn, point, alpha)
            alpha_check.append(alpha)
            if surface[i].PVO(alpha) == 1:
                #s[i].plotRefl(k_refl, b_refl, point)
                print 'Total internal reflection on the', i+1, 'surface'
                break 
            beta = surface[i].beta(alpha)
            beta_check.append(beta)
            [k_refr, b_refr] = surface[i].Refraction(kn, point, beta, k)
            vec = surface[i].create_vec(k_refr, b_refr, point)
            star = point
            #s[i].plotN(kn, bn, point)
            #s[i].plotRefl(k_refl, b_refl, point)
        if point != 'don`t cross':
            if (i == np.size(R)-1) or i == 0:
                x.append(POI)
                y.append((k_refr*x[-1]+b_refr))
                if show:
                    plt.plot(x, y, color ='b')
                picture.append(y[-1])
                
            else:
                if show:
                    plt.plot(x, y, color ='b')
        
        vec[0] = vec0[0]   
        vec[1] = vec0[1] - (j+1)*0.05
#        vec = vec0                                  
#        zero[0] = zero[0]                           
#        zero[1] = zero[1] - 1                       
    spotD = abs(max(picture) - min(picture))
    return spotD

size = SpotD(R, s, vec0, start, POI)
while round(step,10) != 0:
    POI = POI + step
    if SpotD(R, s, vec0, start, POI) >= size and step > 0:
        step = -1*step/2
        POI = POI +step
        size = SpotD(R, s, vec0, start, POI)
    elif SpotD(R, s, vec0, start, POI) >= size and step < 0:
        step = -1*step/2
        POI = POI +step
        size = SpotD(R, s, vec0, start, POI)
    elif SpotD(R, s, vec0, start, POI) < size and step <= 0:
        POI = POI + step
        size = SpotD(R, s, vec0, start, POI)
    elif SpotD(R, s, vec0, start, POI) < size and step >= 0:
        POI = POI + step
        size = SpotD(R, s, vec0, start, POI)
    if POI < d[0]:
        POI = dd
        print 'Diverging beams'
        break
        
SpotD(R, s, vec0, zero, POI, True)
print 'Image plane: ', POI
plt.show() 