import numpy as np
x = [1.0, 2.0, 3.0, 4.0, 5.0]
p = [0.2, 0.2, 0.2, 0.2, 0.2]
L=len(x)
def M(values,probably):
    s=0
    for i in range(L):
        s=s+x[i]*p[i]
    return s
print M(x,p)
print "Done", np.isclose(M(x,p), np.array(x).mean())
def D(values, probaply):
    disp=0
    for i in range(L):
        disp=disp+p[i]*pow(x[i]-M(x,p),2)
    return disp
print D(x,p)
print "Done", np.isclose(D(x,p), np.array(x).std()**2)
