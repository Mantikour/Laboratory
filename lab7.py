# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches
from math import sin, cos, asin

print matplotlib.__version__
#import Functions
M = 10
fig, ax = plt.subplots()
h1 = 7.0
h2 = 5.0
h = min(h1, h2)
R1 = 10.0
teta1 = np.rad2deg(np.arcsin(h/R1))
teta2 = -teta1
d = 2

R2 = 100.0
teta3 = np.rad2deg(np.arcsin(h/R2))
teta4 = -teta3
print teta1, teta2
ax.grid(True, which='both')
ax.axis([-M,M, -M,M])
#ax.axis('equal')
#Arc(xy, width, height, angle=0.0, theta1=0.0, theta2=360.0, **kwargs) 180-teta3, 180-teta4
arc1 = plt.matplotlib.patches.Arc([0, 0], 2*R1, 2*R1, 0.0, 180-teta1, 180+teta1)
arc2 = plt.matplotlib.patches.Arc([-R1-R2+d, 0], 2*R2, 2*R2, 0.0, -teta3, -teta4)
ax.add_artist(arc1)
#ax.add_artist(arc2)

x1 = [-np.sqrt(R1**2 - h**2), np.sqrt(R2**2 - h**2)+(-R1-R2+d)]
y1 = [h, h]

x2 = [-np.sqrt(R1**2 - h**2), np.sqrt(R2**2 - h**2)+(-R1-R2+d)]
y2 = [-h, -h]
#plt.plot(x1, y1)
#plt.plot(x2, y2)
plt.show()
print [-R1-R2+d, 0], 2*R2, 2*R2, 0.0, -teta3, -teta4