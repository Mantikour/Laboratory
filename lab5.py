import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
import Functions
fig, ax = plt.subplots()
n1 = 1.0
n2 = 2.0
n3 = 1.0
x_vert = 2
c1 = [1.0, 0.0]
c2 = [-7.5, 0.0]
xv1 = -10. 
yv1 = 3.
xv2 = -6.
yv2 = 3.
d = [xv2-xv1, yv2-yv1]
R1 = 7
R2 = 10
ax.axis([-12,12, -12,12])
ax.grid(True, which='both')
def degree(angle):
    angle = angle*180/np.pi
    return angle
def radians(angle):
    angle = angle*np.pi/180
    return angle
def beta(angle, n1, n2):
    angle = radians(angle)
    sin_beta = (n1*sin(angle))/n2
    beta = np.arcsin(sin_beta)
    beta = degree(beta)
    return beta
def phi(k1, k2):
    tan_phi = ((k2-k1)/(1+k2*k1))
    phi = np.arctan(tan_phi)
    phi = degree(phi)
    return phi
def find_k(angle, k1):
    angle = radians(angle)
    k2 = ((np.tan(angle))+k1)/(1-(k1*(np.tan(angle))))
    return k2
def find_b(k, point):
    b = point[1]-k*point[0]
    return b

def Circle(R, c):
    circle1 = plt.Circle((c[0], c[1]), R,  color='black', fill=False)
    ax.add_artist(circle1)
    #plt.show()

def plotLine(k, b, first, second):
    x = [first, second]
    y =[]
    for i in range(2):
        y.append(k*x[i] + b)
    ax.plot(x, y)
    #plt.show()
    
def plotRay(vec, pt=[0.0, 0.0], show=True):
    ax.plot([pt[0], vec[0]], [pt[1], vec[1]])
    #if show:
        #plt.show()
        
def Line(x0, y0, x1, y1):
    k = (y1-y0)/(x1-x0)
    b = y0-k*x0
    return k, b

def qua_eq(k, b, R, c):
    b = b + c[0]*k
    D = (2*b*k)**2 - 4*(1+k**2)*(b**2-R**2)
    if D == 0 and k == 0 and b == 0:
        x1 = -R
        x2 = R
        x1 = x1 + c[0]
        x2 = x2 + c[0]
        return x1, x2
    elif D == 0 and k == 0 and b != 0:
        x1 = -np.sqrt(R**2 - b**2)
        x2 = np.sqrt(R**2 - b**2)
        x1 = x1 + c[0]
        x2 = x2 + c[0]
        return x1, x2
    else:
        if D > 0:
            x1 = ((-1)*(2*b*k) - np.sqrt(D))/(2*(1+k**2))
            x2 = ((-1)*(2*b*k) + np.sqrt(D))/(2*(1+k**2))
            x1 = x1 + c[0]
            x2 = x2 + c[0]
            return x1, x2
        else:
            return 'don`t cross'
k1 = Line(xv1, yv1, xv2, yv2)[0]
b1 = Line(xv1, yv1, xv2, yv2)[1]
print k1
print b1      
if qua_eq(k1, b1, R1, c1)=='не пересекаются':
    plotLine(k1, b1, -10, 10)
    Circle(R1, c1)
    raise SystemExit(" don`t cross")
#print qua_eq(k, b, R)
x1 = qua_eq(k1, b1, R1, c1)[0]
y1 = (x1*k1 + b1)+c1[1]
pt = [x1, y1]
print 'точка пересечения', x1, y1
kn = Line(c1[0],c1[1], x1, y1)[0]
bn = Line(c1[0],c1[1], x1, y1)[1]
plotLine(k1, b1, xv1, x1)

Circle(R1, c1)
Circle(R2, c2)
#plotLine(kn, bn, c1[0], x1)                #нормаль к первой точке падения
print 'kn = ', kn
print 'bn = ', bn
xn = x1-c1[0]
yn = y1-c1[1]
global normal 
normal = [xn, yn]
print 'угол падения ', phi(kn, k1)
print 'угол преломления ', beta(phi(kn, k1), n1,n2)
alpha = phi(kn, k1)
k2 = find_k(-alpha, kn) #отраженный луч
b2 = find_b(k2, pt) 
k3 = find_k(beta(phi(kn, k1), n1,n2), kn)#преломленный луч
b3 = find_b(k3, pt)
x2 = qua_eq(k3, b3, R2, c2)[1]
y2 = x2*k3 + b3# + c2[0]*k2
pt2 = [x2, y2]
print pt2
kn2 = Line(c2[0],c2[1], x2, y2)[0]
bn2 = Line(c2[0],c2[1], x2, y2)[1]

if k2>0:
    plotLine(k2, b2, x1, -10)
else:
    plotLine(k2, b2, xv1, x1)
#y3 = k3*x2+b3
#plotLine(kn2, bn2, c2[0], 10) #нормаль ко второй точке падения
plotLine(k3, b3, x1, x2)
alpha2 = phi(kn2, k3)
k4 = find_k(-alpha2, kn2)     #отраженный луч
b4 = find_b(k4, pt2)
k5 = find_k(beta(phi(kn2, k3), n2,n3), kn2)  #преломленный луч
b5 = find_b(k5, pt2)
plotLine(k4, b4, x1, x2)
plotLine(k5, b5, x2, 10)
print 'угол падения ', phi(k3, kn2)
print 'угол преломления ', phi(k5, kn2)
#xh = [x_vert, x_vert]
#yh = []
#yh.append(np.sqrt(R1**2-x_vert**2))
#yh.append(-np.sqrt(R1**2-x_vert**2))
#ax.plot(xh, yh)