import math as m
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
#import Functions
#fig, ax = plt.subplots()
class Surface:
    def __init__ (self, R, c, n_previous, n_next, h):
        self.R = R
        self.c = c
        self.n_previous = n_previous
        self.n_next = n_next
        self.h = h
        
    def plotLine(self, k, b, first, second):
        x = [first, second]
        y = []
        for i in range(2):
            y.append(k*x[i] + b)
        plt.plot(x, y)
        #plt.show()    
    def degree(self, angle):
        angle = angle*180/np.pi
        return angle
    def radians(self, angle):
        angle = angle*np.pi/180
        return angle
    def beta(self, angle):

        angle = np.deg2rad(angle)
        sin_beta = (self.n_previous*(np.sin(angle)))/self.n_next
        beta = np.arcsin(sin_beta)
        #beta = self.degree(beta)
        beta = np.rad2deg(beta)
        return beta
    def phi(self, k1, k2):
        tan_phi = abs((k2-k1)/(1+k2*k1))
        phi = m.atan(tan_phi)
        #phi = self.degree(phi)
        phi = np.rad2deg(phi)
        return phi
    
    def find_k(self, angle, k1):
        angle = self.radians(angle)
        k2 = ((((np.tan(angle)))+k1))/(1-(k1*(np.tan(angle))))
        return k2
    
    def find_b(self, k, point):
        b = point[1]-k*point[0]
        return b
    
    def Circle(self):
        circle1 = plt.Circle((self.c[0], self.c[1]), abs(self.R),  color='black', fill=False)
        ax.add_artist(circle1)
        #plt.show()
    def qua_eq(self, k, b):
        b = b + self.c[0]*k
        D = (2*b*k)**2 - 4*(1+k**2)*(b**2-abs(self.R)**2)
        if D == 0 and k == 0 and b == 0:
            x1 = -abs(self.R)
            x2 = abs(self.R)
            x1 = x1 + self.c[0]
            x2 = x2 + self.c[0]
        elif D == 0 and k == 0 and b != 0:
            x1 = -np.sqrt(abs(self.R)**2 - b**2)
            x2 = np.sqrt(abs(self.R)**2 - b**2)
            x1 = x1 + self.c[0]
            x2 = x2 + self.c[0]
        else:
            if D > 0:
                x1 = ((-1)*(2*b*k) - np.sqrt(D))/(2*(1+k**2))
                x2 = ((-1)*(2*b*k) + np.sqrt(D))/(2*(1+k**2))
                x1 = x1 + self.c[0]
                x2 = x2 + self.c[0]

            else:
                return 'don`t cross'
        if self.R > 0:
            return x1
        else:
            return x2
        
    def Line(self, vec, pt):
        k = vec[1]/vec[0]
        b = pt[1]-k*pt[0]
        return k, b


    def intersection(self, vec, pt):
        k = self.Line(vec, pt)[0]
        b = self.Line(vec, pt)[1]
        xv2 = vec[0] + pt[0]
        x1 = self.qua_eq(k, b)
        if (x1 != 'don`t cross') and (abs(pt[1])<=self.h):
            y1 = (x1*k + b)+self.c[1]
            point = [x1, y1]
            if (x1 - xv2) * (xv2-pt[0]) >= 0:
                return point
            else:
                return 'don`t cross'
        else:
            return 'don`t cross'

    def getN(self, point):
        kn = (self.c[1] - point[1])/(self.c[0] - point[0])
        bn = self.c[1] - kn*self.c[0]
        return kn, bn
    
    def Reflection(self, kn, pt, angle):
        if kn < 0:
            k_refl = self.find_k(-angle, kn)                     
            b_refl = self.find_b(k_refl, pt)
        elif kn >= 0 :
            k_refl = self.find_k(angle, kn)                     
            b_refl = self.find_b(k_refl, pt)
        return k_refl, b_refl
    
    def Refraction(self, kn, pt, angle, k):
        if self.R < 0:
            if kn <= 0 and k <= 0:
                if abs(k) > abs(kn):
                    k_refr = self.find_k(-angle, kn)                    
                    b_refr = self.find_b(k_refr, pt)
                else:
                    k_refr = self.find_k(angle, kn)                    
                    b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            elif kn <= 0 and k > 0:                 
                k_refr = self.find_k(angle, kn)                     
                b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            elif kn >= 0 and k <= 0:
                k_refr = self.find_k(-angle, kn)                     
                b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            elif kn >= 0 and k > 0:
                if abs(k) > abs (kn):
                    k_refr = self.find_k(angle, kn)                   
                    b_refr = self.find_b(k_refr, pt)
                else:
                    k_refr = self.find_k(-angle, kn)                   
                    b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            
        elif self.R > 0:
            if kn < 0 and k < 0:
                if abs(k) > abs(kn):
                    k_refr = self.find_k(-angle, kn)                     
                    b_refr = self.find_b(k_refr, pt)
                else:
                    k_refr = self.find_k(angle, kn)                     
                    b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            elif kn < 0 and k >= 0:
                k_refr = self.find_k(angle, kn)                     
                b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            elif kn >= 0 and k < 0:
                k_refr = self.find_k(-angle, kn)                     
                b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
            elif kn >= 0 and k >= 0:
                if abs(k) > abs(kn):
                    k_refr = self.find_k(angle, kn)                    
                    b_refr = self.find_b(k_refr, pt)
                else:
                    k_refr = self.find_k(-angle, kn)             
                    b_refr = self.find_b(k_refr, pt)
                return k_refr, b_refr
    
    def PVO (self, angle):
        if self.n_previous > self.n_next and np.sin(self.radians(angle))>=self.n_next/self.n_previous:
            pvo = 1
        else:
            pvo = 0
        return pvo

    def plotN(self, kn, bn, pt):
        x = [pt[0], self.c[0]]
        y = []
        for i in range(2):
            y.append(kn*x[i] + bn)
        plt.plot(x, y)
    def plotRefl(self, k_refl, b_refl, point):
        if k_refl or point[1]< 0:
            x = [point[0], point[0]-5]
        else:
            x = [point[0], point[0]+5]
        y = []
        for i in range(2):
            y.append(k_refl*x[i] + b_refl)
        plt.plot(x, y)
    def create_vec(self, k, b, point):
        x1 = point[0]
        y1 = point[1]
        x2 = point[0]+1.0
        y2 = k*x2 + b
        if x2 >= x1:
            x_vec = x2 - x1
            y_vec = y2 - y1
            vec = [x_vec, y_vec]
        else:
            x_vec = x1 - x2
            y_vec = y2 - y1
            vec = [x_vec, y_vec]
        return vec


    def check(self):
        return self.R, self.c, self.n_previous, self.n_next         
    
