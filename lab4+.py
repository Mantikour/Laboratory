import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
n1 = 1.33
n2 = 2.64
n3 = 1.0
K1 = -n1/n2
K2 = -n2/n3
gr = 80                 #угол падения
l=-2
y = float(l)
alpha = float((gr) * np.pi/180)
if n1>n2 and round(np.sin(alpha),2)>=n2/n1 :
    beta = 270
    print  'ПВО на первой поверхности'
else:
    beta = asin(sin(alpha) * n1/n2)
plt.axis('equal')
plt.grid(True, which='both')
plt.axvline(0, c="none")
plt.axhline(0, c="black")
plt.axhline(l, c="black")
global normal
normal = np.array ([0.0,1.0]) 
def plotRay(vec, pt=[0.0, 0.0], show=True):
    plt.plot([pt[0], vec[0]], [pt[1], vec[1]])
    if show:
        plt.show()
    
def Refr(angle, K): 
    d = np.array([np.sin(angle), np.cos(angle)]) 
    C = -np.dot(normal, d)
    if (1-pow(K, 2)*(1-pow(C, 2)))> 0:
        q = (K*d + (K*C - np.sqrt(1-pow(K, 2)*(1-pow(C, 2))))*normal)
    else:
        q = 'не строит'
    return d, q
def Refl(angle, K):
    normal = np.array ([0.0,1.0])
    d = np.array([np.sin(angle), np.cos(angle)])
    r = -d - 2*(np.dot(-d, normal))*normal
    return d, r         
if n1>n2 and np.sin(alpha)>= (n2/n1) and beta==270:
    vp1 = Refl(alpha, K1)[0]
    vo1 = Refl(alpha, K1)[1]
    #plotRay(normal, show=False)
    plotRay(vp1, show=False)
    plotRay(vo1, show=False)
else:
    vp1 = Refr(alpha, K1)[0]
    vpp1 = Refr(alpha, K1)[1]
    vo1 = Refl(alpha, K1)[1]
    #plotRay(normal, show=False)
    plotRay(vp1, show=False)
    plotRay(vo1, show=False)
    plotRay((-l/np.cos(beta))*vpp1, show=False)
if beta != 270:
    if n2>n3 and np.sin(beta)>=n3/n2:
        vp2 = Refl(beta, K2)[0]
        vo2 = Refl(beta, K2)[1]
        #plotRay(normal+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
        plotRay(vo2+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
    else:
        vp2 = Refr(beta, K2)[0]
        vpp2 = Refr(beta, K2)[1]
        vo2 = Refl(beta, K2)[1]
        #plotRay((-l/np.cos(beta))*normal+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
        plotRay((-l/np.cos(beta))*vo2+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
        plotRay(vpp2+[l*np.tan(beta), y], [l*np.tan(beta), y], show=False)
