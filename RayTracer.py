# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, asin
from Lense import *
from Surface import *
M = 200
sc = [0.0, 0.0]
n = [1.0, 1.5, 1.0, 1.33, 1.0, 1.33, 1.0, 1.33, 1.0]
R = [50.0, -65.0, 50.0, -65.0]#, -90.0, 40.0, 50.0, 40.0]
d = [10.0, 20.0, 10.0, 5.0, 10.0, 5.0, 5.0, 5.0]
D = [30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0]
N = 100
vec0 = [3.0, 2.0]
zero = [-60., 0.0]
#vec0 = [3.0, 0.0]
#zero = [-10., 10.0]
#step = 0.005*N
vec = vec0
start = zero
cx =[]
cy = []
s = []
x = [start[0]]
y = [start[1]]
check = []
ax.grid(True, which='both')
#ax.axis([-10,M, -20,20])
#ax.axis('equal')
L = []
s = []
k_check = []
beta_check = []
alpha_check = []
for i in range(np.size(R)-1):
    L.append(Lense(R[i], R[i+1], d[i], D[i], n[i+1], sc))
    L[i].Lense()
    sc[0] = sc[0]+d[i]
cc = 0
dd = 0

for i in range (np.size(R)):
    cc = dd + R[i]
    cx.append(cc)
    cy.append(0.0) 
    dd = dd + d[i]
    
for i in range(np.size(R)):
    s.append(Surface(R[i], [cx[i], cy[i]], n[i], n[i+1], D[i]/2))
    #s[i].Circle()
    
for j in range(N-1):
    start = zero
    x = [start[0]]
    y = [start[1]]
    for i in range(np.size(R)):
        [k, b] = s[i].Line(vec, start)
        point = s[i].intersection(vec, start)
        if point == 'don`t cross':
            #print 'don`t cross'
            break
        x.append(point[0])
        y.append(point[1])
        [kn, bn] = s[i].getN(point)
        alpha = s[i].phi(k, kn)
        [k_refl, b_refl] = s[i].Reflection(kn, point, alpha)
        alpha_check.append(alpha)
        if s[i].PVO(alpha) == 1:
            #s[i].plotRefl(k_refl, b_refl, point)
            print 'Total internal reflection on the', i+1, 'surface'
            break 
        beta = s[i].beta(alpha)
        beta_check.append(beta)
        [k_refr, b_refr] = s[i].Refraction(kn, point, beta, k)
        k = k_refr
        b = b_refr
        vec = s[i].create_vec(k_refr, b_refr, point)
        start = point
        #s[i].plotN(kn, bn, point)
        #s[i].plotRefl(k_refl, b_refl, point)
    if point != 'don`t cross':
        if (k*point[1])>=0:
            if (i == np.size(R)-1) or i == 0:
                x.append(1000)
                y.append((k*x[-1]+b))
                plt.plot(x, y, color ='b')
            else:
                plt.plot(x, y, color ='b')
        else:
            if (i == np.size(R)-1) or i == 0:
                y.append(0.)
                x.append((y[-1]-b)/k)
                check.append((y[-1]-b)/k)
                #print (y[-1]-b)/k
                plt.plot(x, y, color ='b')
            else:
                plt.plot(x, y, color ='b')
        k_check.append(k)
#    else:
#        x.append(zero[0]+vec0[0])
#        y.append((k*x[-1]+b))
#        plt.plot(x, y, color ='b')
    vec[0] = vec0[0]   
    vec[1] = vec0[1] - (j+1)*0.05
#    vec = vec0 
#    zero[0] = zero[0]
#    zero[1] = zero[1] - step
print np.mean(check)
print np.std(check)
   
plt.show()