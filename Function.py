def degree(angle):
    angle = angle*180/np.pi
    return angle
def radians(angle):
    angle = angle*np.pi/180
    return angle
def beta(angle, n1, n2):
    angle = radians(angle)
    sin_beta = (n1*sin(angle))/n2
    beta = np.arcsin(sin_beta)
    beta = degree(beta)
    return beta
def phi(k1, k2):
    tan_phi = abs((k2-k1)/(1+k2*k1))
    phi = np.arctan(tan_phi)
    phi = degree(phi)
    return phi
def find_k(angle, k1):
    angle = radians(angle)
    k2 = ((np.tan(angle))+k1)/(1-(k1*(np.tan(angle))))
    return k2
def find_b(k, point):
    b = point[1]-k*point[0]
    return b
def Circle(R, c):
    circle1 = plt.Circle((c[0], c[1]), R,  color='black', fill=False)
    ax.add_artist(circle1)
    #plt.show()
def plotLine(k, b, first, second):
    x = [first, second]
    y =[]
    for i in range(2):
        y.append(k*x[i] + b)
    ax.plot(x, y)
    #plt.show()
def Line(x0, y0, x1, y1):
    k = (y1-y0)/(x1-x0)
    b = y0-k*x0
    return k, b
def qua_eq(k, b, R, c):
    D = (2*b*k)**2 - 4*(1+k**2)*(b**2-R**2)
    if D == 0 and k == 0 and b == 0:
        x1 = -R
        x2 = R
        x1 = x1 + c[0]
        x2 = x2 + c[0]
        return x1, x2
    elif D == 0 and k == 0 and b != 0:
        x1 = -np.sqrt(R**2 - b**2)
        x2 = np.sqrt(R**2 - b**2)
        x1 = x1 + c[0]
        x2 = x2 + c[0]
        return x1, x2
    else:
        if D > 0:
            x1 = ((-1)*(2*b*k) - np.sqrt(D))/(2*(1+k**2))
            x2 = ((-1)*(2*b*k) + np.sqrt(D))/(2*(1+k**2))
            x1 = x1 + c[0]
            x2 = x2 + c[0]
            return x1, x2
        else:
            return 'don`t cross'